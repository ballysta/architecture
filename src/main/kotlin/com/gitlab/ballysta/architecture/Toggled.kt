@file:Suppress("NOTHING_TO_INLINE")
package com.gitlab.ballysta.architecture

import java.awt.event.ComponentEvent

interface Toggled {
    val onEnabled: Event<() -> (Unit)>
    val onDisabled: Event<() -> (Unit)>

    val enabled: Boolean

    operator fun <Return>
        ((Toggled).() -> (Return))
        .invoke()
        = this(this@Toggled)
    operator fun <Return, First>
        ((Toggled).(First) -> (Return))
        .invoke(first: First)
        = this(this@Toggled, first)
    operator fun <Return, First, Second>
        ((Toggled).(First, Second) -> (Return))
        .invoke(first: First, second: Second)
        = this(this@Toggled, first, second)
    operator fun <Return, First, Second, Third>
        ((Toggled).(First, Second, Third) -> (Return))
        .invoke(first: First, second: Second, third: Third)
        = this(this@Toggled, first, second, third)
}

interface Togglable : Toggled {
    override var enabled: Boolean
}
inline fun Togglable.enable() { enabled = true }
inline fun Togglable.disable() { enabled = false }

inline val Toggled.whenEnabled get(): Event<() -> (Unit)>
    = { if (enabled) it(); onEnabled(it) }
inline val Toggled.whenDisabled get(): Event<() -> (Unit)>
    = { if (!enabled) it(); onDisabled(it) }

val phased: Toggled.(Togglable.() -> (Unit)) -> (Togglable) = {
    val parent = this; val logic = Component(it)
    parent.onDisabled(logic, logic::disable)
    object : Togglable by logic {
        override var enabled: Boolean
            get() = logic.enabled
            set(value) { logic.enabled = parent.enabled && value }
    }
}
val transient: Toggled.(Togglable.() -> (Unit)) -> (Togglable) = {
    block -> Component(block).also {
        whenDisabled(it, it::disable)
        whenEnabled(it, it::enable)
    }
}

fun Toggled.ComponentEvent()
        = object : TreeEvent<() -> (Unit)>() {
    //FIXME this may be a potential CME
    override fun invoke(scope: Toggled, listener: () -> Unit) {
        if (scope === this@ComponentEvent) {
            if (iterating) changes.add(index++ to listener)
            else listeners[index++] = listener
        } else super.invoke(scope, listener)
    }
}

fun Component(block: Togglable.() -> (Unit)): Togglable = object : Togglable {
    override val onEnabled = ComponentEvent()
    override val onDisabled = ComponentEvent()

    override var enabled = false
        set(enable) {
            if (field == enable) return
            field = enable
            if (enable) onEnabled()
            else onDisabled()
        }

    init { block(this) }
}