@file:Suppress("NOTHING_TO_INLINE", "DEPRECATION", "DEPRECATED_IDENTITY_EQUALS")
package com.gitlab.ballysta.architecture

import java.util.concurrent.ConcurrentHashMap
import kotlin.collections.HashMap
import kotlin.reflect.KProperty

//TODO contract to imply that either to or from is not null.
typealias ChangeEvent<Key, Value> = Event<(Key, Value?, Value?) -> (Unit)>

interface Mutated<Key, Value> {
    val onChanged: ChangeEvent<Key, Value>
    operator fun get(key: Key): Value?

    val keys: Enumerator<Key>
    val values: Enumerator<Value>

    val size get() = keys.reduce(0) { a, _ -> a + 1 }
}
interface Mutable<Key, Value> : Mutated<Key, Value> {
    operator fun set(key: Key, value: Value?): Value?
}

//TODO add key and value holder methods.

//--Table (map like)--
typealias Table<Key, Value> = Mutated<Key, Value>
typealias MutableTable<Key, Value> = Mutable<Key, Value>
fun <Key, Value> MutableTable() = object : Mutable<Key, Value> {
    val map = ConcurrentHashMap<Key, Value>()
    override val onChanged = TreeEvent<(Key, Value?, Value?) -> (Unit)>()
    override fun get(key: Key) = map[key]
    override fun set(key: Key, value: Value?): Value? {
        val current = map[key!!]
        if (value != current) {
            onChanged(key, current, value)
            when (value) {
                null -> map.remove(key)
                else -> map[key] = value
            }
        }
        return current
    }

    override val keys get() = map.entries.enumerator().map { it.key }
    override val values get() = map.entries.enumerator().map { it.value }
    override val size get() = map.size
}
fun <Key, Value> mutableTableOf(vararg entries: Pair<Key, Value>) = MutableTable<Key, Value>().also {
    entries.forEach { (key, value) -> it[key] = value }
}
inline operator fun <Value> Table<Value, *>.contains(value: Value) = get(value) != null
inline fun <Key, Value> Table<Key, Value>.forEach(block: (Key, Value) -> (Unit)) {
    val keys = keys
    val values = values
    var key = keys()
    while (key != null) {
        block(key, values()!!)
        key = keys()
    }
}
inline fun <Key, Value> Mutated<Key, Value>.all(filter: (Key, Value) -> (Boolean)): Boolean {
    forEach { key, value ->  if (!filter(key, value)) return false }
    return true
}

val <Key, Value> Table<Key, Value>.onAdded get(): Event<(Key, Value) -> (Unit)>
    = { onChanged { key, _, to -> if (to != null) it(key, to) } }
val <Key, Value> Table<Key, Value>.onRemoved get(): Event<(Key, Value) -> (Unit)>
    = { onChanged { key, from, _ -> if (from != null) it(key, from) } }

val <Key, Value> Mutated<Key, Value>.onEach get() : Toggled.(Toggled.(Key, Value) -> (Unit)) -> (Unit) = {
    val components = HashMap<Key, Togglable>()
    onChanged { key, from, to ->
        if (from != null) components.remove(key)?.disable()
        if (to != null) {
            val component = Component { it(key, to) }
            components[key] = component
            component.enable()
        }
    }
    onDisabled { components.values.removeIf { it.disable(); true } }
    onEnabled {
        forEach { key, value ->
            val component = Component { it(key, value) }
            components[key] = component
            component.enable()
        }
    }
}
val <Key, Value> Mutated<Key, Value>.cache
    get(): Toggled.() -> (Mutated<Key, Value>) = {
        object : Mutated<Key, Value> {
            val map = HashMap<Key, Value>()
            init {
                onEnabled { this@cache.forEach(map::put) }
                this@cache.onChanged { key, _, to ->
                    if (to == null) map -= key else map[key] = to
                }
                onDisabled { map.clear() }
            }
            override val onChanged = this@cache.onChanged
            override fun get(key: Key) = map[key]
            override val keys get() = map.keys.enumerator()
            override val values get() = map.values.enumerator()
            override val size = map.size
        }
    }
fun <Key, Value> Table<Key, Value>.watch(target: PublishedObservable<Key>) = object : PublishedObservable<Value?> {
    override val last get() = get(target.last)

    override fun invoke(scope: Toggled, observer: (Value?) -> (Unit)) = scope.run {
        target { observer(get(it)) }
        this@watch.onChanged { key, _, to -> if (target.last == key) observer(to) }
    }
}
fun <Key, Value> Table<Key, Value>.watch(target: Key) = watch(just(target))


//--Holder (set like)--
typealias Holder<Value> = Mutated<Value, Value>
typealias MutableHolder<Value> = Mutable<Value, Value>
inline fun <Value> MutableHolder() = MutableTable<Value, Value>()

inline fun <Value> MutableHolder<Value>.add(value: Value) = set(value, value) == null
inline fun <Value> MutableHolder<Value>.remove(value: Value) = set(value, null) != null
inline operator fun <Value> MutableHolder<Value>.plusAssign(value: Value)  { set(value, value) }
inline operator fun <Value> MutableHolder<Value>.minusAssign(value: Value) { set(value, null) }


//--Index (list like)--
typealias Index<Value> = Mutated<Int, Value>
typealias MutableIndex<Value> = Mutable<Int, Value>

fun <Value> MutableIndex<Value>.push(index: Int, value: Value) {
    val current = set(index, value)
    if (current != null && index < size)
        push(index + 1, current)
}
fun <Value> MutableIndex<Value>.pop(index: Int): Value? {
    //implement this
    return null
}
inline fun <Value> MutableIndex<Value>.pushBack(value: Value) = push(size - 1, value)
inline fun <Value> MutableIndex<Value>.pushFront(value: Value) = push(0, value)
inline fun <Value> MutableIndex<Value>.popBack() = pop(size - 1)
inline fun <Value> MutableIndex<Value>.popFront() = pop(0)


//--Single (object like)--
typealias Single<Value> = Mutated<Unit, Value>
typealias MutableSingle<Value> = Mutable<Unit, Value>
fun <Value> MutableSingle(defaultValue: Value? = null) = object : MutableSingle<Value> {
    override val onChanged = TreeEvent<(Unit, Value?, Value?) -> (Unit)>()
    var current = defaultValue

    override fun get(key: Unit) = current
    override fun set(key: Unit, value: Value?) = current.also {
        current = value; onChanged(Unit, it, value)
    }

    override val keys = object : Enumerator<Unit> {
        var hasCalled = false
        override fun invoke(): Unit? {
            return if (!hasCalled) {
                hasCalled = true;
                if (current != null) Unit else null
            } else null
        }
    }
    override val values = object : Enumerator<Value> {
        var hasCalled = false
        override fun invoke(): Value? {
            return if (!hasCalled) {
                hasCalled = true; current
            } else null
        }
    }

    override val size get() = if (current == null) 0 else 1
}
inline fun <Value> Single(value: Value?) = object : Single<Value> {
    override fun get(key: Unit) = value
    override val onChanged: ChangeEvent<Unit, Value> = {}
    override val size = if (value == null) 0 else 1
    override val keys = object : Enumerator<Unit> {
        var hasCalled = false
        override fun invoke(): Unit? {
            return if (!hasCalled) {
                hasCalled = true; Unit
            } else null
        }
    }
    override val values = object : Enumerator<Value> {
        var hasCalled = false
        override fun invoke(): Value? {
            return if (!hasCalled) {
                hasCalled = true; value
            } else null
        }
    }
}

inline fun <Value> Single<Value>.watch() = watch(Unit)

inline operator fun MutableSingle<Int>.plusAssign(value: Int) { set((get() ?: 0) + value) }

inline fun <Value> MutableSingle<Value>.set(value: Value?) = set(Unit, value)
inline fun <Value> Single<Value>.get() = get(Unit)
inline operator fun <Value> MutableSingle<Value>.getValue(
    instance: Any?, property: KProperty<*>
) = get()
inline operator fun <Value> MutableSingle<Value>.setValue(
    instance: Any?, property: KProperty<*>, value: Value?
) = set(value)