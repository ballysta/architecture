@file:Suppress("NOTHING_TO_INLINE")
package com.gitlab.ballysta.architecture

import java.util.concurrent.ForkJoinPool
import java.util.concurrent.ForkJoinTask
import javax.security.auth.Subject
import kotlin.time.Duration
import kotlin.time.ExperimentalTime
import kotlin.time.TimeMark
import kotlin.time.TimeSource.Monotonic.markNow

typealias Observable<Type> = Event<(Type) -> (Unit)>

fun <Type, To> Observable<Type>.compose(
    composition: ((To) -> (Unit)).(Type) -> (Unit)
): Observable<To> = { listener ->
    invoke(this) { value -> composition(listener, value) }
}

inline fun <Type, To> Observable<Type>.map(
    noinline mapper: (Type) -> (To)
) = compose<Type, To> { this(mapper(it)) }

inline fun <Type, To> Observable<Type>.mapNotNull(
    noinline mapper: (Type) -> (To?)
) = compose<Type, To> { mapper(it)?.also(this) }

inline fun <Type> Observable<Type>.filter(
    noinline filter: (Type) -> (Boolean)
) = compose<Type, Type> { if (filter(it)) this(it) }

@ExperimentalTime fun <Type> Observable<Type>.debounce(
    duration: Duration
): Observable<Type> {
    var last: TimeMark? = null
    return compose {
        if (last == null || last!!.hasPassedNow()) {
            last = markNow() + duration; this(it)
        }
    }
}
fun <Type> Observable<Type>.count(): Observable<Int> {
    var count = 0; return { count++ }
}

typealias Scheduler = (() -> (Unit)) -> (Unit)
val io: Scheduler = { task ->
    ForkJoinPool.commonPool().submit(task)
}
fun <Type> Observable<Type>.observeOn(
    scheduler: (() -> (Unit)) -> (Unit)
): Observable<Type> = { observer ->
    this@observeOn { scheduler { observer(it) } }
}
fun <Type> Observable<Type>.subscribeOn(
    scheduler: (() -> (Unit)) -> (Unit)
): Observable<Type> = { observer ->
    scheduler { Component {
        this@subscribeOn { observer(it) }
    } }
}


interface PublishedObservable<Type> : (Toggled, (Type) -> (Unit)) -> (Unit) {
    val last: Type

    fun <To> map(mapper: (Type) -> (To)): PublishedObservable<To> {
        val parent = this
        return object : PublishedObservable<To> {
            override val last get() = mapper(parent.last)
            override fun invoke(scope: Toggled, observer: (To) -> (Unit))
                = parent.invoke(scope) { observer(mapper(it)) }
        }
    }
}
fun <Type> Observable<Type>.publish(initial: Type): Toggled.() -> (PublishedObservable<Type>) = {
    object : PublishedObservable<Type> {
        override var last = initial

        val event = TreeEvent<(Type) -> (Unit)>()
        override fun invoke(scope: Toggled, observer: (Type) -> (Unit)) {
            event(scope, observer)
            scope.onEnabled(scope) { observer(last) }
        }

        init { this@publish {
            val current = last; event(it)
            if (current == last) last = it
        } }
    }
}
val <Type> PublishedObservable<Type>.onNext get(): Toggled.((Type, Type) -> (Unit)) -> (Unit) = {
    this@onNext { value -> it(this@onNext.last, value) }
}
val <Type> PublishedObservable<Type>.onChanged get(): Toggled.((Type, Type) -> (Unit)) -> (Unit) = {
    onNext { from, to -> if (from !=  to) it(from, to) }
}
fun <Type, To> Observable<Type>.aggregate(
    seed: To, aggregator: (To, Type) -> (To)
) = object : PublishedObservable<To> {
    override var last = seed
    override fun invoke(scope: Toggled, observer: (To) -> (Unit))
        = scope.run { this@aggregate {
            val next = aggregator(last, it)
            observer(next); last = next
        } }
}

fun <Type> just(value: Type) = object : PublishedObservable<Type> {
    override val last = value
    override fun invoke(scope: Toggled, observer: (Type) -> (Unit)) {
        scope.onEnabled(scope) { observer(last) }
    }
}

