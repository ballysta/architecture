@file:Suppress("NOTHING_TO_INLINE")
package com.gitlab.ballysta.architecture

typealias Enumerator<Type> = () -> (Type?)

inline fun <Type> Iterator<Type>.toEnumerator(): Enumerator<Type>
    = { if (hasNext()) next() else null }
inline fun <Type> Array<Type>.enumerator(): Enumerator<Type>
    { var i = 0; return { if (i < size) get(i++) else null } }
inline fun <Type> Iterable<Type>.enumerator()
    = iterator().toEnumerator()
inline fun <Type> enumeratorOf(vararg values: Type)
    = values.enumerator()
inline fun <Type> emptyEnumerator(): Enumerator<Type> = { null }
inline fun <Type, To> Enumerator<Type>.map(
    noinline mapper: (Type) -> (To)
): Enumerator<To> = { this@map()?.let(mapper) }
inline fun <Type> Enumerator<Type>.filter(
    noinline filter: (Type) -> (Boolean)
): Enumerator<Type> = {
    var next = this@filter()
    while (next != null && !filter(next))
        next = this@filter()
    next
}
inline fun <Type> Enumerator<Type>.forEach(
    block: (Type) -> (Unit)
) {
    var value = this@forEach()
    while (value != null) {
        block(value)
        value = this@forEach()
    }
}

fun <Type, To> Enumerator<Type>.reduce(
    seed: To, reducer: (To, Type) -> (To)
): To {
    val value = this@reduce()
    return if (value == null) seed else
    reduce(reducer(seed, value), reducer)
}

operator fun <Type> Enumerator<Type>.iterator() = object : Iterator<Type> {
    fun fetchNext() = this@iterator.invoke()
    var next = fetchNext()
    override fun hasNext() = next != null
    override fun next() = next?.apply { next = fetchNext() }!!
}
